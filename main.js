
    angular.module('myApp', ['ngSanitize'])

    .controller('myController', function($scope, $http){

        $scope.load = function(category_id){

            $scope.questions = null;
            $scope.current_question = 0;
            $scope.show_result = false;       

            $http.get('https://opentdb.com/api.php?amount=10&category='+category_id+'&difficulty=easy').then(function(response){

                $scope.questions = response.data.results;
                for (var i = 0; i < $scope.questions.length; i++) {
                    
                    $scope.questions[i].display_answers = 
                        shuffle($scope.questions[i].incorrect_answers
                                .concat([$scope.questions[i].correct_answer]))
                    
                }

            })
        }

        $scope.categories = [
			{id:9 , name: "General Knowledge"},
            {id:10, name: "Entertainment: Books"},
            {id:11, name: "Entertainment: Film"},
            {id:12, name: "Entertainment: Music"},
            {id:14, name: "Entertainment: Television"},
            {id:15, name: "Entertainment: Video Games"},
            {id:29, name: "Entertainment: Comics"},            
            {id:17, name: "Science & Nature"},
            {id:18, name: "Science: Computers"},
            {id:19, name: "Science: Mathematics"},
            {id:30, name: "Science: Gadgets"},            
            {id:20, name: "Mythology"},
            {id:21, name: "Sports"},
            {id:22, name: "Geography"},
            {id:23, name: "History"},
            {id:24, name: "Politics"},
            {id:25, name: "Art"},
            {id:26, name: "Celebrities"},
            {id:27, name: "Animals"},
            {id:28, name: "Vehicles"},                                                                    
        ]

        $scope.getMargin = function(){

            return $scope.current_question * -100 + 'vh'

        }

        $scope.next = function(){
            if($scope.current_question == $scope.questions.length -1){
                $scope.show_result = true;
            }else{
                $scope.current_question++;
            }
            
        }

        $scope.back = function(){
            $scope.current_question--;
        }      

        $scope.getScore = function(){
            var score = 0;
            for (var i = 0; i < $scope.questions.length; i++) {
                var question = $scope.questions[i]

                if(question.display_answers[question.selected] == question.correct_answer){
                    score++;
                }

            }
            return score;
        }        

        $scope.reset = function(){
            $scope.questions = null;
            $scope.current_question = 0;
            $scope.show_result = false;            
        }

        function shuffle(a) {
            var j, x, i;
            for (i = a.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = a[i];
                a[i] = a[j];
                a[j] = x;
            }
            return a;
        }        

    })
